package game;

import java.awt.*;

public class Line {
    //起点坐标
    private int x=380;
    private int y=180;
    //终点坐标
    private int endx=500;
    private int endy=500;
    //线长
    private double length = 100;
    //线长最小值
    private double MIN_length = 100;
    //线长最大值
    private double MAX_length = 750;
    //角度百分比
    private double n = 0;
    //方向
    private int dir = 1;

    //状态 0 摇摆 1 抓取 2 收回 3 抓取返回
    private int state;
    //钩爪图片
    Image hook = Toolkit.getDefaultToolkit().getImage("imgs/hook.png");

    GameWin frame;

    Line(GameWin frame){this.frame=frame;}

    //碰撞检测,检测物体是否被抓取
    void logic(){
        for(Object obj:this.frame.objectList){
            if(endx>obj.getX() && endx<obj.getX()+obj.getWidth()
                    && endy>obj.getY() && endy< obj.getY()+obj.getHeight()){
                state=3;
                obj.setFlag(true);
            }
        }

    }
    //绘制方法
    void lines(Graphics g){
        endx = (int) (x +length*Math.cos(n* Math.PI));
        endy = (int) (y +length*Math.sin(n*Math.PI));
        g.setColor(Color.red);
        g.drawLine(x-1,y,endx-1,endy);
        g.drawLine(x,y,endx,endy);
        g.drawLine(x+1,y,endx+1,endy);
        g.drawImage(hook,endx-36,endy-2,null);
    }

    void paintSelf(Graphics g){
        logic();
        switch (state){
            case 0:
                if(n<0.1){ dir = 1;}
                else if (n>0.9){dir = -1;}
                n=n+0.005*dir;
                lines(g);
                break;
            case 1:
                if(length<=MAX_length){
                    length=length+5;
                    lines(g);
                }else {state=2;}
                break;
            case 2:
                if(length>=MIN_length){
                    length=length-5;
                    lines(g);
                }else {
                    state=0;
                }
                break;
            case 3://抓取返回
                int m=1;
                if(length>=MIN_length){
                    length=length-5;
                    lines(g);
                    for(Object obj:this.frame.objectList){
                        if(obj.isFlag()){
                            m=obj.getM();
                            obj.setX(endx-obj.getWidth()/2);
                            obj.setY(endy);
                            if(length<=MIN_length){
                                obj.setX(-150);
                                obj.setY(-150);
                                obj.setFlag(false);
                                //加分
                                Bg.setCount(Bg.getCount()+obj.getCount());
                                Bg.setWaterFlag(false);
                                state=0;
                            }
                            if(Bg.isWaterFlag()){
                                if(obj.getType()==1){
                                    //金块
                                    m=1;
                                }else if(obj.getType()==2){
                                    //石块
                                    obj.setX(-150);
                                    obj.setY(-150);
                                    obj.setFlag(false);
                                    Bg.setWaterFlag(false);
                                    state=2;
                                }
                            }
                        }
                    }
                }
                try {
                    Thread.sleep(m);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;

                default:
        }
    }
    //重置线
    void reGame(){
        n=0;
        length=100;
    }
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getEndx() {
        return endx;
    }

    public void setEndx(int endx) {
        this.endx = endx;
    }

    public int getEndy() {
        return endy;
    }

    public void setEndy(int endy) {
        this.endy = endy;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getMIN_length() {
        return MIN_length;
    }

    public void setMIN_length(double MIN_length) {
        this.MIN_length = MIN_length;
    }

    public double getMAX_length() {
        return MAX_length;
    }

    public void setMAX_length(double MAX_length) {
        this.MAX_length = MAX_length;
    }

    public double getN() {
        return n;
    }

    public void setN(double n) {
        this.n = n;
    }

    public int getDir() {
        return dir;
    }

    public void setDir(int dir) {
        this.dir = dir;
    }
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
