package game;

import java.awt.*;

public class Gold extends Object {

    Gold(){
        this.setX((int)(Math.random()*700));
        this.setY((int)(Math.random()*550+300));
        this.setWidth(52);
        this.setHeight(52);
        this.setFlag(false);
        this.setM(30);
        this.setCount(4);
        this.setType(1);
        this.setImg(Toolkit.getDefaultToolkit().getImage("imgs/gold1.gif"));
    }
}

class GoldMini extends Gold{
    GoldMini(){
        this.setWidth(36);
        this.setHeight(36);
        this.setM(15);
        this.setCount(2);
        this.setImg(Toolkit.getDefaultToolkit().getImage("imgs/gold0.gif"));
    }
}

class GoldPlus extends Gold{
    GoldPlus(){
        this.setX((int)(Math.random()*650));
        this.setWidth(105);
        this.setHeight(105);
        this.setM(60);
        this.setCount(8);
        this.setImg(Toolkit.getDefaultToolkit().getImage("imgs/gold2.gif"));
    }
}

