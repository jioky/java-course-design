package game;

import java.awt.*;

public class Object {


    //坐标
    private int x;
    private int y;
    //宽高
    private int width;
    private int height;
    //图片
    private Image img;
    //标记,是否能移动
    private boolean flag;
    //质量
    private int m;
    //积分
    private int count;
    //标记 1为金块,2为石块
    private int type;
    //获取矩形
	public Rectangle getRec(){
	    return new Rectangle(x,y,width,height);
	}

	public int getY() {
        return y;
    }
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Image getImg() {
        return img;
    }

    public void setImg(Image img) {
        this.img = img;
    }


    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }



    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }



    void paintSelf(Graphics g){
        g.drawImage(img,x,y,null);
    }

    public int getWidth() {
        return width;
    }

}
