package game;

import java.awt.*;

public class Bg {

    //关卡数
    private static int level = 1;
    //目标得分
    private int goal = level*10;
    //总分
    private static int count=0;
    // 药水数量
    private static int waterNum = 3;
    //药水状态,默认F,T 正在使用 F 无法使用
    private static boolean waterFlag = false;
    //开始时间
    private long startTime;
    //结束时间
    private long endTime;
    //药水价格
    private int price=6;
    //是否进入商店
    private boolean shop=false;





    //载入图片
    Image bg = Toolkit.getDefaultToolkit().getImage("imgs/bg.jpg");
    Image bg1 = Toolkit.getDefaultToolkit().getImage("imgs/bg1.jpg");
    Image peo = Toolkit.getDefaultToolkit().getImage("imgs/peo.png");
    Image water = Toolkit.getDefaultToolkit().getImage("imgs/water.png");

    //绘制
    void paintSelf(Graphics g){
        g.drawImage(bg1,0,0,null);
        g.drawImage(bg,0,200,null);
        switch (GameWin.state){
            case 0:
                drawWord(g,80,Color.green,"准备开始",200,400);
                break;
            case 1:
                g.drawImage(peo,310,50,null);
                drawWord(g,30,Color.black,"积分:"+count,30,150);
                //药水组件
                g.drawImage(water,450,40,null);
                drawWord(g,30,Color.black,"*"+waterNum,510,70);
                //关卡数
                drawWord(g,20,Color.black,"第"+level+"关",30,60);
                //目标积分
                drawWord(g,30,Color.black,"目标"+goal,30,110);
                //时间组件
                endTime = System.currentTimeMillis();
                long tim = 20-(endTime-startTime)/1000;
                drawWord(g,30,Color.black,"时间:"+(tim>0?tim:0),520,150);
                break;
            case 2:
                g.drawImage(water,300,400,null);
                drawWord(g,30,Color.black,"价格:"+price,300,500);
                drawWord(g,30,Color.black,"是否购买:"+count,300,550);
                if (isShop()){
                    setCount(getCount()-getPrice());
                    setWaterNum(getWaterNum()+1);
                    setShop(false);
                    GameWin.state=1;
                    startTime=System.currentTimeMillis();
                }
                break;
            case 3:
                drawWord(g,80,Color.cyan,"失败",250,350);
                drawWord(g,80,Color.cyan,"积分:"+count,200,450);
                break;
            case 4:
                drawWord(g,80,Color.red,"成功",250,350);
                drawWord(g,80,Color.red,"积分:"+count,200,450);
                break;
            default:
        }


    }
    //t倒计时完成 f正在倒计时
    boolean gameTime(){
        long tim = (endTime-startTime)/1000;
        if(tim>20){return true;}
        return false;

    }

    //重置元素
    void reGame(){
        //关卡数
        level = 1;
        //目标得分
        goal = level*10;
        //总分
        count=0;
        //药水数量
        waterNum = 3;
        //药水状态,默认F,T 正在使用 F 无法使用
        waterFlag = false;
    }
    //绘制字符串
    public static void drawWord(Graphics g,int size,Color color,String str,int x,int y){
        g.setColor(color);
        g.setFont(new Font("仿宋",Font.BOLD,size));
        g.drawString(str,x,y);
    }
    public static int getLevel() {
        return level;
    }

    public static void setLevel(int level) {
        Bg.level = level;
    }

    public int getGoal() {
        return goal;
    }

    public void setGoal(int goal) {
        this.goal = goal;
    }



    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Bg.count = count;
    }



    public static int getWaterNum() {
        return waterNum;
    }

    public static void setWaterNum(int waterNum) {
        Bg.waterNum = waterNum;
    }



    public static boolean isWaterFlag() {
        return waterFlag;
    }

    public static void setWaterFlag(boolean waterFlag) {
        Bg.waterFlag = waterFlag;
    }



    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isShop() {
        return shop;
    }

    public void setShop(boolean shop) {
        this.shop = shop;
    }

}
