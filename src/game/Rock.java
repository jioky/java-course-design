package game;

import java.awt.*;

public class Rock extends Object {
    Rock(){
        this.setX((int)(Math.random()*700));
        this.setY((int)(Math.random()*550+300));
        this.setWidth(71);
        this.setHeight(71);
        this.setFlag(false);
        this.setM(50);
        this.setCount(1);
        this.setType(2);
        this.setImg(Toolkit.getDefaultToolkit().getImage("imgs/rock1.png"));
    }
}
